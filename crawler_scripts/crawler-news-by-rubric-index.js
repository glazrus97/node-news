// Пробный скрипт - получает атрибуты новостей с главное страницы

let Crawler = require("crawler");

let indexNews = [];

let c = new Crawler({
    maxConnections: 10,
    // This will be called for each crawled page
    callback: function (error, res, done) {
        if (error) {
            console.log(error);
        } else {
            let $ = res.$;
            // $ is Cheerio by default
            $(".story").each(
                function () {
                    let title = $(this).find(".story__title .link").text();
                    let url = "https://yandex.ru" + $(this).find(".story__title .link").attr("href");
                    let news = {
                        title: title,
                        url: url,
                        rubric_id: 1,
                        createdAt: new Date(),
                        updatedAt: new Date()
                    };
                    indexNews.push(news);
                }
            )
        }
        done();
    }
});

c.queue('https://yandex.ru/news/rubric/index?from=index');

c.on('drain',function(){
    console.log(indexNews);
});