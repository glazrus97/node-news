// Пробный скрипт - получает атрибуты новостей с главное страницы

let Crawler = require("crawler");

let c = new Crawler({
    maxConnections: 10,
    // This will be called for each crawled page
    callback: function (error, res, done) {
        if (error) {
            console.log(error);
        } else {
            let $ = res.$;
            // $ is Cheerio by default
            //a lean implementation of core jQuery designed specifically for the server
            $(".story").each(
                function () {
                    let rubric = $(this).find(".rubric-label").text();
                    //Можно идентифицировать рубрику по её урлу
                    let rubricURL = $(this).find(".rubric-label").attr("href");
                    let title = $(this).find(".story__title .link").text();
                    // текст есть не у всех новостей на этой страницы, его лучше получать из конкретной страницы новости
                    // let text = $(this).find(".story__text").text();
                    let link = "https://yandex.ru" + $(this).find(".story__title .link").attr("href");
                    console.log(rubric, title, rubricURL, link);
                }
            )
        }
        done();
    }
});

c.queue('https://yandex.ru/news/');