// Пробный скрипт - получает рубрики новостей с главное страницы

let Crawler = require("crawler");

let c = new Crawler({
    maxConnections: 10,
    // This will be called for each crawled page
    callback: function (error, res, done) {
        if (error) {
            console.log(error);
        } else {
            let $ = res.$;
            // $ is Cheerio by default
            $(".nav-by-rubrics .tabs-menu .nav-by-rubrics__rubric").each(
                function () {
                    let name = $(this).find(".link").text();
                    let url = $(this).find(".link").attr("href");
                    let rubric = {
                        name: name,
                        url: url,
                        createdAt: new Date(),
                        updatedAt: new Date()
                    };
                }
            )
        }
        done();
    }
});

c.queue('https://yandex.ru/news/');