'use strict';
module.exports = (sequelize, DataTypes) => {
  const Rubrics = sequelize.define('Rubrics', {
    name: DataTypes.STRING,
    url: DataTypes.STRING
  }, {});
  Rubrics.associate = function(models) {
    // associations can be defined here
  };
  return Rubrics;
};