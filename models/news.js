'use strict';
module.exports = (sequelize, DataTypes) => {
  const News = sequelize.define('News', {
    title: DataTypes.STRING,
    url: DataTypes.STRING,
    rubric_id: DataTypes.INTEGER
  }, {});
  News.associate = function(models) {
    // associations can be defined here
  };
  return News;
};