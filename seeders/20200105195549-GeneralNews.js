'use strict';

let generalNews = [{
  title: 'Иран отказался от ограничений ядерной сделки',
  url:
    'https://yandex.ru/news/story/Iran_otkazalsya_ot_ogranichenij_yadernoj_sdelki--fc5d62e021cea584e072cca7a8fffabd?lr=213&lang=ru&stid=S9gloONLUNKO9cbKCIO9&persistent_id=83949635&rubric=index&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'Эрдоган объявил о начале переброски турецких военных в Ливию',
  url:
    'https://yandex.ru/news/story/EHrdogan_obyavil_o_nachale_perebroski_tureckikh_voennykh_v_Liviyu--39cf1280d2a2022ba2f1c825b9417ca3?lr=213&lang=ru&stid=7F0Uxhk-F3jACfMc_gkR&persistent_id=84016734&rubric=index&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Канада — Россия 1:2 (2-й перерыв)',
  url:
    'https://yandex.ruhttps://yandex.ru/sport/story/Sbornaya_Rossii_povela_v_schyote_finala_MCHM_s_Kanadoj--7d9c5a938d6b955e1924657bac1e52c5?lr=213&lang=ru&stid=Jfc5VZZJd3ovDHsXudad&persistent_id=84016044&rubric=index&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Парламент Ирака потребовал вывода войск коалиции США',
  url:
    'https://yandex.ru/news/story/Parlament_Iraka_potreboval_vyvoda_vojsk_koalicii_SSHA--eeb2fc761bed6ff5c4db2c75168d74f4?lr=213&lang=ru&stid=vH4AmbpRKZSSsuKIyiXO&persistent_id=83945138&rubric=index&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'Мужчин, спасших женщину от грабителя в Нижегородской области, наградят',
  url:
    'https://yandex.ru/news/story/Muzhchin_spasshikh_zhenshhinu_ot_grabitelya_v_Nizhegorodskoj_oblasti_nagradyat--9fe0412f1de4a80dc1b4806183d187a7?lr=213&lang=ru&stid=q9Q_FKrkL8vUbQamln5H&persistent_id=83942039&rubric=index&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'Собянин: Северо-восточный участок БКЛ метро откроется к 2022 году',
  url:
    'https://yandex.ru/news/story/Sobyanin_Severo-vostochnyj_uchastok_BKL_metro_otkroetsya_k_2022_godu--2f17d2fbd25cd406d568fed2f5caae42?lr=213&lang=ru&stid=_PcBaDX_RMgKgSs6x5KT&persistent_id=83947366&rubric=Moscow&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: '379 пунктов приема елок начали работать в Москве',
  url:
    'https://yandex.ru/news/story/379_punktov_priema_elok_nachali_rabotat_v_Moskve--945b2230a6eee24998b14af961bab70a?lr=213&lang=ru&stid=_loHTqV7XbFvyQB-bNnk&persistent_id=83661107&rubric=Moscow&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'Мосгаз обновит системы газоснабжения в 30 тысячах квартир в 2020 году',
  url:
    'https://yandex.ru/news/story/Mosgaz_obnovit_sistemy_gazosnabzheniya_v_30_tysyachakh_kvartir_v_2020_godu--524bdb539be13173431c687799fd48e8?lr=213&lang=ru&stid=8_Wyjno173MBrl2CbNCy&persistent_id=83940643&rubric=Moscow&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'Количество высоток по программе реновации в Москве составляет 3%',
  url:
    'https://yandex.ru/news/story/Kolichestvo_vysotok_po_programme_renovacii_v_Moskve_sostavlyaet_3--211a2d0793c7dce38a72f9da1f2b9c93?lr=213&lang=ru&stid=ElG87Pa3TM8aNj9-zU1d&persistent_id=83865454&rubric=Moscow&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Москвичей ждет аномально теплое Рождество',
  url:
    'https://yandex.ru/news/story/Moskvichej_zhdet_anomalno_teploe_Rozhdestvo--1ecbf925a117b58899f32c46299d94d2?lr=213&lang=ru&stid=hrIA45o24zim&persistent_id=84017579&rubric=Moscow&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'В Крыму призвали США не множить мифы о полуострове',
  url:
    'https://yandex.ru/news/story/V_Krymu_prizvali_SSHA_ne_mnozhit_mify_o_poluostrove--bc4a4b831dd9a1971acd73a05eaf94ec?lr=213&lang=ru&stid=y4fcgPWMjNQ6bz-Da65W&persistent_id=83941587&rubric=personal_feed&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'В Тюменской области в ДТП погибла семья с тремя детьми',
  url:
    'https://yandex.ru/news/story/V_Tyumenskoj_oblasti_v_DTP_pogibla_semya_s_tremya_detmi--9f7769ceee7c8405fabceaf21c195869?lr=213&lang=ru&stid=BPzK1ekqkV7fogcx9IpL&persistent_id=83898551&rubric=personal_feed&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Гороскоп для всех знаков Зодиака на 4 января 2020 года',
  url:
    'https://yandex.ru/news/story/Goroskop_dlya_vsekh_znakov_Zodiaka_na_4_yanvarya_2020_goda--3c69b2aef818653ac83f6fd9fc9608f7?lr=213&lang=ru&stid=aZePQioqPecDB1EQXtIa&persistent_id=83305832&rubric=personal_feed&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'Глава Минздрава выразила соболезнования из-за смерти онколога Павленко',
  url:
    'https://yandex.ru/news/story/Glava_Minzdrava_vyrazila_soboleznovaniya_iz-za_smerti_onkologa_Pavlenko--45ffcf38a3728cbe62e90a4634eaaf28?lr=213&lang=ru&stid=9J3HND7u6FrM6IbyPKWt&persistent_id=83945339&rubric=personal_feed&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'Назван состав сборной России по хоккею на финальный матч МЧМ с Канадой',
  url:
    'https://yandex.ruhttps://yandex.ru/sport/story/Nazvan_sostav_sbornoj_Rossii_po_khokkeyu_na_finalnyj_match_MCHM_s_Kanadoj--5012f72760984c8f20895b47f0c46c17?lr=213&lang=ru&stid=7os81mpGJ-jRrgx5kfYV&persistent_id=83898064&rubric=personal_feed&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'Гуайдо лишился поста руководителя Национальной ассамблеи Венесуэлы',
  url:
    'https://yandex.ru/news/story/Guajdo_lishilsya_posta_rukovoditelya_Nacionalnoj_assamblei_Venesuehly--1e236d3a07697154522e9e3b3a0c785b?lr=213&lang=ru&stid=nQIjdLPZGP7xvg5PACwq&persistent_id=84014204&rubric=politics&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Трамп пригрозил Ирану ударом новейшим оружием',
  url:
    'https://yandex.ru/news/story/Tramp_prigrozil_Iranu_udarom_novejshim_oruzhiem--86b06f226e4276c65cce4271ab08b100?lr=213&lang=ru&stid=YHkITXcLo0X69L-57EjU&persistent_id=83929671&rubric=politics&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'МИД РФ призвал Украину перестать глумиться над историей',
  url:
    'https://yandex.ru/news/story/MID_RF_prizval_Ukrainu_perestat_glumitsya_nad_istoriej--9fa2e12ff35340147929d2acfba61cb9?lr=213&lang=ru&stid=peXSI5bZiF5aRd5-VOQ0&persistent_id=83939406&rubric=politics&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Зеленский с семьей уехал отдыхать в Оман',
  url:
    'https://yandex.ru/news/story/Zelenskij_s_semej_uekhal_otdykhat_v_Oman--b1faba03730ecf1b2c0f4fe127f77f48?lr=213&lang=ru&stid=daj67Cw9YHij_zoFtkCX&persistent_id=83945242&rubric=politics&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Путин обеспокоился снижением доходов россиян',
  url:
    'https://yandex.ru/news/story/Putin_obespokoilsya_snizheniem_dokhodov_rossiyan--6cb16d7e557c51a1f17ec086098a46b4?lr=213&lang=ru&stid=iAAFUKmgQGxNmnGiEoD9&persistent_id=83528831&rubric=politics&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Россиянам спрогнозировали аномальное тепло на Рождество',
  url:
    'https://yandex.ru/news/story/Rossiyanam_sprognozirovali_anomalnoe_teplo_na_Rozhdestvo--d2f9b052c046bb6e6aa79bed923cb81f?lr=213&lang=ru&stid=S6a6yuMbYdvi6dF7Skxq&persistent_id=83946756&rubric=society&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Умер хирург-онколог Андрей Павленко',
  url:
    'https://yandex.ru/news/story/Umer_khirurg-onkolog_Andrej_Pavlenko--cb1a51e51dbee040f2bdb48cebb34742?lr=213&lang=ru&stid=NmGPrX2Da81VzzGOogCU&persistent_id=83934038&rubric=society&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'О повышении зарплат россиян в 2020 году рассказали работодатели',
  url:
    'https://yandex.ru/news/story/O_povyshenii_zarplat_rossiyan_v_2020_godu_rasskazali_rabotodateli--a7f8a7e69a065003987639577758d1be?lr=213&lang=ru&stid=GxV4VwVrplQDMTfOu3P6&persistent_id=83923266&rubric=society&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'Северная Македония и Греция уже получают газ по «Турецкому потоку»',
  url:
    'https://yandex.ru/news/story/Severnaya_Makedoniya_i_Greciya_uzhe_poluchayut_gaz_po_Tureckomu_potoku--8ae034ae131bd4b3da52defb25f73b86?lr=213&lang=ru&stid=ybsIut6AkM4EZXSskmm-&persistent_id=83535157&rubric=society&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'В России планируют изменить возраст вступления в брак',
  url:
    'https://yandex.ru/news/story/V_Rossii_planiruyut_izmenit_vozrast_vstupleniya_v_brak--8707bb98acd860ebaa6611deddd0760f?lr=213&lang=ru&stid=xgsuATkQgFe7xQ98jK4S&persistent_id=83887853&rubric=society&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Оператор ГТС Украины отреагировал на данные о транзите',
  url:
    'https://yandex.ru/news/story/Operator_GTS_Ukrainy_otreagiroval_na_dannye_o_tranzite--52b18f29794732b673affc7bd53ea140?lr=213&lang=ru&stid=jpWxCCh-q3j4Cy-YO4p0&persistent_id=83931837&rubric=business&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'Акции Saudi Aramco упали до минимума на фоне убийства Сулеймани',
  url:
    'https://yandex.ru/news/story/Akcii_Saudi_Aramco_upali_do_minimuma_na_fone_ubijstva_Sulejmani--8b278d96e9d4dfe2443d2c006bf5a48c?lr=213&lang=ru&stid=BEXNGikvJv-XPq4Sqcf0&persistent_id=84004386&rubric=business&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Россия остановила поставки нефти в Белоруссию',
  url:
    'https://yandex.ru/news/story/Rossiya_ostanovila_postavki_nefti_v_Belorussiyu--8c5f6e97ca48782a3d46a108c1232566?lr=213&lang=ru&stid=fh_uCSFKAzl6PowilL1I&persistent_id=83803369&rubric=business&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'Reuters узнал о передаче иностранцам контроля над нефтью в Венесуэле',
  url:
    'https://yandex.ru/news/story/Reuters_uznal_o_peredache_inostrancam_kontrolya_nad_neftyu_v_Venesuehle--8eb6960825fb6149ac33f60c41097788?lr=213&lang=ru&stid=FuRKrh1_DJDjIaPpcbTJ&persistent_id=83850079&rubric=business&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'Миллиардер Агаларов раскритиковал низкие пенсии и зарплаты россиян',
  url:
    'https://yandex.ru/news/story/Milliarder_Agalarov_raskritikoval_nizkie_pensii_i_zarplaty_rossiyan--63082fa76b56749af172f1493bd00883?lr=213&lang=ru&stid=8YTBO057slD7iq7SEVUC&persistent_id=83747894&rubric=business&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Иран готовит удар по военным объектам США',
  url:
    'https://yandex.ru/news/story/Iran_gotovit_udar_po_voennym_obektam_SSHA--0b4c9adb03dc9627f45bc8205e77efe0?lr=213&lang=ru&stid=WeUu_vv-gk9viCccAg5s&persistent_id=83949603&rubric=world&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'Сайт призывной службы США рухнул после убийства Сулеймани Вашингтоном',
  url:
    'https://yandex.ru/news/story/Sajt_prizyvnoj_sluzhby_SSHA_rukhnul_posle_ubijstva_Sulejmani_Vashingtonom--0977a9e96a78f5f81f9beccf16d56279?lr=213&lang=ru&stid=b5YUSGbgN695rt_u1Row&persistent_id=83875053&rubric=world&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Ученые назвали помогающий снизить давление напиток',
  url:
    'https://yandex.ru/news/story/Uchenye_nazvali_pomogayushhij_snizit_davlenie_napitok--460808f7f154f6ad5f0f2ff0affcc0e8?lr=213&lang=ru&stid=Gi_R_gLRaUf2iJz1xMAJ&persistent_id=83899508&rubric=world&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'В аэропорту Стамбула запретили встречать пассажиров с табличками',
  url:
    'https://yandex.ru/news/story/V_aehroportu_Stambula_zapretili_vstrechat_passazhirov_s_tablichkami--ab60e57bd8107ca26e5f3f3d2f047ef9?lr=213&lang=ru&stid=KKXpzNrew3tdK5lyCAwt&persistent_id=83890236&rubric=world&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'В Хорватии сегодня пройдет второй тур президентских выборов',
  url:
    'https://yandex.ru/news/story/V_KHorvatii_segodnya_projdet_vtoroj_tur_prezidentskikh_vyborov--674f073d8e8d81fe073756cf89f24676?lr=213&lang=ru&stid=8OGxGOfk58y3xm8G0Map&persistent_id=83835937&rubric=world&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Россиянин Большунов выиграл «Тур де Ски»',
  url:
    'https://yandex.ruhttps://yandex.ru/sport/story/Rossiyanin_Bolshunov_vyigral_Tur_de_Ski--ddbec367363678226758447a1316b32f?lr=213&lang=ru&stid=ja1yYqMZOveL3it78ZuW&persistent_id=84008883&rubric=sport&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: '«Ливерпуль» обыграл «Эвертон» в матче Кубка Англии',
  url:
    'https://yandex.ruhttps://yandex.ru/sport/story/Liverpul_obygral_EHverton_v_matche_Kubka_Anglii--9f8e3b835dd7693c4f6a57ae5e19ea8c?lr=213&lang=ru&stid=hcm3aPtI22RG-bR2CuJw&persistent_id=83868236&rubric=sport&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    '«Ак Барс» обыграл «Трактор», отыгравшись в третьем периоде с 0:3',
  url:
    'https://yandex.ruhttps://yandex.ru/sport/story/Ak_Bars_obygral_Traktor_otygravshis_v_tretem_periode_s_03--940649b90e67da883d09d2421befa5a6?lr=213&lang=ru&stid=DxgGTYxQ8i0S8_86-quz&persistent_id=83924742&rubric=sport&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'Сборная Германии разгромила Казахстан и сохранила место в элите МЧМ',
  url:
    'https://yandex.ruhttps://yandex.ru/sport/story/Sbornaya_Germanii_razgromila_Kazakhstan_i_sokhranila_mesto_v_ehlite_MCHM--d02337b222e4b8bb4838ceac7ab21907?lr=213&lang=ru&stid=_vv_jTT4WN1dHIhI3E6C&persistent_id=83892766&rubric=sport&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'Фазель подтвердил, что МЧМ по хоккею 2023 года пройдет в России',
  url:
    'https://yandex.ruhttps://yandex.ru/sport/story/Fazel_podtverdil_chto_MCHM_po_khokkeyu_2023_goda_projdet_v_Rossii--5aa4f1f775942a6b19ad27984b5357fd?lr=213&lang=ru&stid=htHal_dKr3GquvyCzixM&persistent_id=84002187&rubric=sport&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'В Таиланде задержали россиян за секс на пляже',
  url:
    'https://yandex.ru/news/story/V_Tailande_zaderzhali_rossiyan_za_seks_na_plyazhe--08ebbb666ecfd49820822d8d8f8c1c51?lr=213&lang=ru&stid=K7VP9tb8qcmFvFjPh-HW&persistent_id=83934526&rubric=incident&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'В Крыму пьяный полицейский сбил пешеходов на переходе',
  url:
    'https://yandex.ru/news/story/V_Krymu_pyanyj_policejskij_sbil_peshekhodov_na_perekhode--08afb606c29e1ee621146d97048f6f4b?lr=213&lang=ru&stid=N7q9DW9SKn73wGR1HUSy&persistent_id=84008738&rubric=incident&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Украинские военные подорвались на своей мине в Донбассе',
  url:
    'https://yandex.ru/news/story/Ukrainskie_voennye_podorvalis_na_svoej_mine_v_Donbasse--1ec5bb4d2ce68a256f68bf3f78e47c64?lr=213&lang=ru&stid=C40cl13A04ei7vZJG8i9&persistent_id=83945886&rubric=incident&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'Модель обвинила Никаса Сафронова в изнасиловании и попала в СИЗО',
  url:
    'https://yandex.ru/news/story/Model_obvinila_Nikasa_Safronova_v_iznasilovanii_i_popala_v_SIZO--a9aa634353aadc6eb3380f7ae7d6a577?lr=213&lang=ru&stid=7PLCJy5tsPB2C56n0A7m&persistent_id=83887732&rubric=incident&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Двух пропавших в Киеве девушек нашли мертвыми в шкафу',
  url:
    'https://yandex.ru/news/story/Dvukh_propavshikh_v_Kieve_devushek_nashli_mertvymi_v_shkafu--40d87351594df5011e9f71cd97982d0d?lr=213&lang=ru&stid=bLSyNSh9cR7EwDOxSmfA&persistent_id=83901535&rubric=incident&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Скончался народный артист России Геннадий Пискунов',
  url:
    'https://yandex.ru/news/story/Skonchalsya_narodnyj_artist_Rossii_Gennadij_Piskunov--ad79370c4e31cb62ab4b8f0559dfe312?lr=213&lang=ru&stid=CHmeTSMs9_QMnWztrfdf&persistent_id=84007866&rubric=culture&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Кассовые сборы российских фильмов снизились в 2019 году',
  url:
    'https://yandex.ru/news/story/Kassovye_sbory_rossijskikh_filmov_snizilis_v_2019_godu--a2b80b1ddbca4c6c0b42b3f0010d83c9?lr=213&lang=ru&stid=g7VCqQ5hpDyu0lhr9gB0&persistent_id=83828355&rubric=culture&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'Мультфильм «Холодное сердце 2» стал самым кассовым в истории',
  url:
    'https://yandex.ru/news/story/Multfilm_KHolodnoe_serdce_2_stal_samym_kassovym_v_istorii--bce8ba53c642855ed53908cfbf327f71?lr=213&lang=ru&stid=P9mD&persistent_id=84017405&rubric=culture&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'Назван лучший фильм 2019 года, по мнению американских кинокритиков',
  url:
    'https://yandex.ru/news/story/Nazvan_luchshij_film_2019_goda_po_mneniyu_amerikanskikh_kinokritikov--438671a5d5c2a46a2ba4213a9d617912?lr=213&lang=ru&stid=Pd0mhYZzgeNra-WvZE7X&persistent_id=83941730&rubric=culture&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Первые зрители высоко оценили «Вторжение» Бондарчука',
  url:
    'https://yandex.ru/news/story/Pervye_zriteli_vysoko_ocenili_Vtorzhenie_Bondarchuka--5e643f4ec1e12119397b4a20dcf8be06?lr=213&lang=ru&stid=rqrwy9_WLkmKWs8PwDJ0&persistent_id=83945203&rubric=culture&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'В Microsoft прокомментировали прекращение поддержки Windows 7',
  url:
    'https://yandex.ru/news/story/V_Microsoft_prokommentirovali_prekrashhenie_podderzhki_Windows_7--b87d88aa334369b3f1350f3d6bf97b63?lr=213&lang=ru&stid=q4G68lBr-gFToGZ3kkyA&persistent_id=83414056&rubric=computers&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'Xiaomi рассказала, что будет делать в случае санкций со стороны США',
  url:
    'https://yandex.ru/news/story/Xiaomi_rasskazala_chto_budet_delat_v_sluchae_sankcij_so_storony_SSHA--f47216c3fc7e178a6f1b0dd89dee0f9f?lr=213&lang=ru&stid=oak9m7rIFX4reXuFIPW7&persistent_id=83943170&rubric=computers&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'Samsung представила доступные Galaxy Note10 Lite и Galaxy S10 Lite',
  url:
    'https://yandex.ru/news/story/Samsung_predstavila_dostupnye_Galaxy_Note10_Lite_i_Galaxy_S10_Lite--d51568a7f840743d7a8e6dc367078434?lr=213&lang=ru&stid=SZPluwQJa9IJBYkxuH2_&persistent_id=83763311&rubric=computers&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Названа распространенная ошибка при зарядке смартфона',
  url:
    'https://yandex.ru/news/story/Nazvana_rasprostranennaya_oshibka_pri_zaryadke_smartfona--87044365527687092ba3b423df7642df?lr=213&lang=ru&stid=o8v8w1e7SGVOmDVcdc_0&persistent_id=83250068&rubric=computers&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'Убийца Nvidia GeForce GTX 1660 Super от AMD выйдет 21 января',
  url:
    'https://yandex.ru/news/story/Ubijca_Nvidia_GeForce_GTX_1660_Super_ot_AMD_vyjdet_21_yanvarya--10836bd60bc27e531c38bb4a4dfa77bc?lr=213&lang=ru&stid=2aKYMW8I&persistent_id=84018437&rubric=computers&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Разработана вакцина против болезни Альцгеймера',
  url:
    'https://yandex.ru/news/story/Razrabotana_vakcina_protiv_bolezni_Alcgejmera--52021beb911cd6171776c718db945aa0?lr=213&lang=ru&stid=cHENb5eXRNRI9SWA2-KK&persistent_id=83639800&rubric=science&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Тупики пользуются орудиями для получения желаемого',
  url:
    'https://yandex.ru/news/story/Tupiki_polzuyutsya_orudiyami_dlya_polucheniya_zhelaemogo--0138fdac966057982f40f4fe65d254a0?lr=213&lang=ru&stid=gY2vjqTbhIriJxok&persistent_id=83640010&rubric=science&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Построен тепловой квантовый двигатель',
  url:
    'https://yandex.ru/news/story/Postroen_teplovoj_kvantovyj_dvigatel--e7bccd3b45ec1770174de0f0c4c029de?lr=213&lang=ru&stid=4inUw-6bbvcfEolM&persistent_id=83648059&rubric=science&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Вакцина от гриппа может способствовать борьбе с раком',
  url:
    'https://yandex.ru/news/story/Vakcina_ot_grippa_mozhet_sposobstvovat_borbe_s_rakom--1c193715510356c9f5f1ed3cbd088449?lr=213&lang=ru&stid=ft8zA8y2eHy27XL1V8QN&persistent_id=83673020&rubric=science&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'Конечности животных могли сформироваться из кожного скелета плавников',
  url:
    'https://yandex.ru/news/story/Konechnosti_zhivotnykh_mogli_sformirovatsya_iz_kozhnogo_skeleta_plavnikov--8fff11f9c46e716730c4191ec7f4890c?lr=213&lang=ru&stid=Ghr-pcDd&persistent_id=83754217&rubric=science&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title:
    'В России назвали пять новых автомобилей дешевле полумиллиона',
  url:
    'https://yandex.ru/news/story/V_Rossii_nazvali_pyat_novykh_avtomobilej_deshevle_polumilliona--c91f8a7a5c0224ff102b94d28978c40b?lr=213&lang=ru&stid=BP6gfPLMLFRY4jXSCqN6&persistent_id=83809946&rubric=auto&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Kia запретила продавать новый седан Optima таксистам',
  url:
    'https://yandex.ru/news/story/Kia_zapretila_prodavat_novyj_sedan_Optima_taksistam--84b463799d976c180b39f5b99f0dc1ca?lr=213&lang=ru&stid=sPInK754aSEkNhZkBJOD&persistent_id=83906461&rubric=auto&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Skoda рассказала о новиках для России в 2020 году',
  url:
    'https://yandex.ru/news/story/Skoda_rasskazala_o_novikakh_dlya_Rossii_v_2020_godu--77077e9dd26a5532f76822a81016f0a2?lr=213&lang=ru&stid=dpBmsRfpLUjF&persistent_id=83916171&rubric=auto&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Что изменится для автомобилистов России в 2020 году',
  url:
    'https://yandex.ru/news/story/CHto_izmenitsya_dlya_avtomobilistov_Rossii_v_2020_godu--43ff77dbb5ea5b737f660f93dd943c20?lr=213&lang=ru&stid=ZCDGlcFqFTMIy8DKFxNU&persistent_id=83745405&rubric=auto&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  title: 'Продажи премиальных автомобилей в РФ выросли на 4%',
  url:
    'https://yandex.ru/news/story/Prodazhi_premialnykh_avtomobilej_v_RF_vyrosli_na_4--dc1fb19bdeed7afbfcaec3548a7bddda?lr=213&lang=ru&stid=AydfNJzwd7xL3pcXGS1y&persistent_id=83871956&rubric=auto&from=index',
  rubric_id: 1,
  createdAt: new Date(),
  updatedAt: new Date()
}];


module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('News', generalNews, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('News', null, {});
  }
};
