'use strict';

let Rubrics = [{
  name: 'Главное',
  url: 'https://yandex.ru/news/rubric/index?from=index',
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  name: 'Москва',
  url: 'https://yandex.ru/news/region/Moscow?from=index',
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  name: 'Рекомендуем',
  url: 'https://yandex.ru/news/rubric/personal_feed?from=index',
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  name: 'Политика',
  url: 'https://yandex.ru/news/rubric/politics?from=index',
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  name: 'Общество',
  url: 'https://yandex.ru/news/rubric/society?from=index',
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  name: 'Экономика',
  url: 'https://yandex.ru/news/rubric/business?from=index',
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  name: 'В мире',
  url: 'https://yandex.ru/news/rubric/world?from=index',
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  name: 'Спорт',
  url: 'https://yandex.ru/sport?from=index',
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  name: 'Происшествия',
  url: 'https://yandex.ru/news/rubric/incident?from=index',
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  name: 'Культура',
  url: 'https://yandex.ru/news/rubric/culture?from=index',
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  name: 'Технологии',
  url: 'https://yandex.ru/news/rubric/computers?from=index',
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  name: 'Наука',
  url: 'https://yandex.ru/news/rubric/science?from=index',
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  name: 'Авто',
  url: 'https://yandex.ru/news/rubric/auto?from=index',
  createdAt: new Date(),
  updatedAt: new Date()
}];

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Rubrics', Rubrics, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Rubrics', null, {});
  }
};
